#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on Feb 24, 2019
@author Dmitrij Nepachalovich
'''

import sys
import argparse

import sales
import users
import helpers.paths as path_helper
#import helpers.printer as print_helper

class App():
    def __init__(self):
        self.user = None
        self.managerRoleFlag = None
        self.beverage = None
        self.ingridients = []
        self.price = None
        
    def getCmdlnValues(self, cmdlnInput):
        parser = argparse.ArgumentParser()
        parser.add_argument('name')
        parser.add_argument('-m', '--manager', action='store_true')
        parser.add_argument('-b', '--beverage')
        parser.add_argument('-i', '--ingridients', nargs='+', default='')
        parser.add_argument('-p', '--price', type=int)
        try:
            cmdlnValues = parser.parse_args(cmdlnInput)
        except Exception as e:
            print (e)
            return None
        else:
            self.user = cmdlnValues.name
            self.managerRoleFlag = cmdlnValues.manager
            self.beverage = cmdlnValues.beverage
            self.ingridients = cmdlnValues.ingridients
            self.price = cmdlnValues.price
            
    def doSalesmanActiities(self, User):
        sale = sales.Sale(self.beverage, self.price, self.ingridients)
        sale.createSale(User)
        sales.printSaleBill()
        sales.printSaleBillToFile()
        
    def doManagerActivities(self):
        sales.printSalesBill() 
            
    def dispatchUserActivitiesByRole(self, User):
        print ('Hello, {u.name}! You are identified as {u.role}!'.format(u=User))
        User.updateUsersList()
        
        if (User.role == users.SALESMAN_ROLE):
            self.doSalesmanActiities(User)
       
        elif (User.role == users.MANAGER_ROLE):
            self.doManagerActivities()
        
        else:
            raise Exception ('Undefined user role')
            return None 
    

def main():
    path_helper.defineAppPath()
    appInstance = App()
    appInstance.getCmdlnValues(sys.argv[1:])    
    activeUser = users.User(appInstance.user, appInstance.managerRoleFlag) 
    appInstance.dispatchUserActivitiesByRole(activeUser)  
    
if __name__ == '__main__':
    main()