#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on Feb 24, 2019
@author Dmitrij Nepachalovich
'''

import helpers.paths as path_helper
import helpers.database as db_helper
import helpers.bills as bill_helper

class Sale:
    
    def __init__(self, beverage, price, ingridients):
        self.beverage = beverage
        self.price = price
        self.ingridients = ingridients
        
    def createSale(self, User):
        dbCall = db_helper.DbConn()
        # update beverages table
        dbCall.updateBeveragesTable(self.beverage)
        # create sale record in db
        dbCall.createNewSale(self, User)

    
def getSalesBill():
    billTemplate = bill_helper.SalesBill()
    # prepare report data
    dbCall = db_helper.DbConn()
    # required report data wrapped as dictionary
    billData = dict()
    billData['sales'] = dbCall.selectSales()
    billData['totalSales'] = dbCall.selectTotalSales()
    billData['totalValue'] = dbCall.selectTotalValue()
    # send and get report data
    report = billTemplate.SalesBillOutput(billData)
    # return report data as list of strings
    return report

def getSaleBill():
    billTemplate = bill_helper.SaleBill()
    # prepare report data
    dbCall = db_helper.DbConn()
    # required report data wrapped as dictionary
    billData = dict()
    billData['user'] = dbCall.selectUserBySale()
    billData['beverage'] = dbCall.selectBeverageBySale()
    billData['ingridients'] = dbCall.selectIngridientsBySale()
    billData['price'] = dbCall.selectSalePrice()
    # send and get report data
    report = billTemplate.SaleBillOutput(billData)
    # return report data as list of strings
    return report
        
def printSalesBill():
    billOutput = getSalesBill()
    for s in billOutput:
        print(s)
        
def printSaleBill():
    billOutput = getSaleBill()
    for s in billOutput:
        print(s)
        
def printSaleBillToFile():
        path_helper.setPathToBills()
        billFilename = path_helper.getBillFilename()
        billFile = open(billFilename, 'wb')
        billOutput = getSaleBill()
        for s in billOutput:
            billFile.write(s + '\n')
        billFile.close()
        

        