#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on Feb 24, 2019
@author Dmitrij Nepachalovich
'''

import sys
import argparse
import helpers.paths as path_helper
import helpers.database as db_helper

def main():
    path_helper.defineAppPath()                                     # define app path and set it to global variable using helper
    parser = argparse.ArgumentParser()                              # get parameters from command line
    parser.add_argument('-c', '--cleardb', action='store_true')     # define cmd line flag for deleting tables from db
    param = parser.parse_args(sys.argv[1:])                         # parse parameters
    dbCall = db_helper.DbConn()
    if (param.cleardb):                                             # if flag =true
        dbCall.deleteTables()                                       # call function from helper to delete tables 
    dbCall.createTables()                                           # create new tables in db using helper
    
if __name__ == '__main__':
    main()