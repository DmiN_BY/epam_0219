#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on Feb 24, 2019
@author Dmitrij Nepachalovich
'''

import helpers.database as db_helper

SALESMAN_ROLE = 'Salesman'
MANAGER_ROLE = 'Manager'

class User:
    def __init__(self, user, role):
        self.name = user
        try:
            if (role):
                self.role = MANAGER_ROLE
            else:
                self.role = SALESMAN_ROLE
        except Exception as e:
            print (e)
        return None
    
    def updateUsersList(self):
        dbCall = db_helper.DbConn()
        dbCall.updateUsersTable(self.name, self.role) 

