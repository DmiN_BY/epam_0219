#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on Feb 24, 2019
@author Dmitrij Nepachalovich
'''

import unittest
from mock import patch

import sales

class SalesTest(unittest.TestCase):
    
    def setUp(self):
        self.beverage = 'testBeverage'
        self.ingridients = ['testIngridient1', 'testIngridient2']
        self.price = 1
        
class SalesConstructorTest(SalesTest):
        
    def test_SaleSuccesfullyCreatedWithExpectedBeverage(self):
        expResult = self.beverage
        testSale = sales.Sale(self.beverage, '', '')
        actResult = testSale.beverage
        self.assertEqual(actResult, expResult, 'Beverage in created sale is not match to expected!')
        
    def test_SaleSuccesfullyCreatedWithExpectedIngridients(self):
        expResult = self.ingridients
        testSale = sales.Sale('', '', self.ingridients)
        actResult = testSale.ingridients
        self.assertEqual(actResult, expResult, 'Ingridients in created sale is not match to expected!')
        
    def test_SaleSuccesfullyCreatedWithExpectedPrice(self):
        expResult = self.price
        testSale = sales.Sale('', self.price, '')
        actResult = testSale.price
        self.assertEqual(actResult, expResult, 'Price in created sale is not match to expected!')
        
@patch('helpers.database.DbConn')
class CreateSaleMethodTest(SalesTest):
        
    
    def test_CreateSaleCallsDbConnection(self, mock_DbConn):
        testSale = sales.Sale('', '', '')
        testSale.createSale('')
        self.assertTrue(mock_DbConn.called, 'DbConn.__init__ is not called!')
        
    def test_CreateSaleCallsCreateNewSale(self, MockDbConn):
        DbConn = MockDbConn()
        testSale = sales.Sale('', '', '')
        testSale.createSale('')
        self.assertTrue(DbConn.createNewSale.called, 'DbConn.createNewSale is not called!')
        
    def test_CreateSaleCallsUpdateBeveragesTable(self, MockDbConn):
        DbConn = MockDbConn()
        testSale = sales.Sale('', '', '')
        testSale.createSale('')
        self.assertTrue(DbConn.updateBeveragesTable.called, 'DbConn.updateBeveragesTable is not called!')

if __name__ == '__main__':
    unittest.main()
        