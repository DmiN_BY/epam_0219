#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on Feb 24, 2019
@author Dmitrij Nepachalovich
'''

import unittest
from mock import patch

import users

class UsersTest(unittest.TestCase):

    
    def setUp(self):
        self.testUserName = 'tUser'
        
class UserConstructoTest(UsersTest):
        
    def test_CreatedUserNameIsEqualToRequestedUserName(self):
        expResult = self.testUserName
        testUser = users.User(expResult, False)
        actResult = testUser.name
        self.assertEqual(actResult, expResult, 'Created user name differs from requested user name')

    def test_SalesmanUserRoleSuccessfullyCreatedWhenUserRoleFlagIsFalse(self):
        expResult = users.SALESMAN_ROLE
        testUser = users.User(self.testUserName, False)
        actResult = testUser.role
        self.assertEqual(actResult, expResult, 'Created user role differs from expected (salesman) user role')
        
    def test_ManagerUserRoleSuccessfullyCreatedWhenUserRoleFlagIsTrue(self):
        expResult = users.MANAGER_ROLE
        testUser = users.User(self.testUserName, True)
        actResult = testUser.role
        self.assertEqual(actResult, expResult, 'Created user role differs from expected (manager) user role')
        
    def test_UserCreatingExceptionRaisesOnUndefinedUserRoleFlag(self):  
        self.assertRaises(Exception, testUser = users.User(self.testUserName, ''))
        
@patch('helpers.database.DbConn')        
class UpdateUsersTableMethodTest(UsersTest):
        
    def test_updateUsersListCallsUpdateUsersTable(self, MockDbConn):
        DbConn = MockDbConn()
        testUser = users.User(self.testUserName, True)
        testUser.updateUsersList()
        self.assertTrue(DbConn.updateUsersTable.called, 'DbConn.updateUserTable is not called!')
           
    def test_updateUsersListCallsDbConnection(self, mock_DbConn):
        testUser = users.User(self.testUserName, True)
        testUser.updateUsersList()
        self.assertTrue(mock_DbConn.called, 'DbConn.__init__ is not called!')
        
if __name__ == '__main__':
    unittest.main()