#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on Feb 24, 2019
@author Dmitrij Nepachalovich
'''


_PAGE_WIDTH = 60
_PRINT_WIDTH = _PAGE_WIDTH - 2
_VERTICAL_PLACEHOLDER = '|'
_HORIZONTAL_PLACEHOLDER_THICK = '='
_HORIZONTAL_PLACEHOLDER_THIN = '-'
_CORNER_PLACEHOLDER = '+'

_HEADERS = ['Seller name', 'Number of sales', 'Total Value ($)']
_ERROR_MESSAGE = 'Insufficient page width for printing data!'

class Bill:
    def __init__(self, pageWidth, headers):
        self.pageWidth = pageWidth
        self.headers = headers
        self._CORNER_PLACEHOLDER = _CORNER_PLACEHOLDER
        self.columnWidths = []
        columnsWidth = 0
        for h in self.headers:
            hLength = len(h)
            self.columnWidths.append(hLength)
            columnsWidth = columnsWidth + hLength
        self.minimalWidth = columnsWidth + len(self.headers)+1
        if (self.pageWidth >= self.minimalWidth):
            self.columnWidths[0] = self.columnWidths[0] + self.pageWidth - self.minimalWidth
        else:
            print (_ERROR_MESSAGE)
            return None
        
    def HorizontalLine(self, fillWith):
        outputLine = '{c}{:{fill}^{width}}{c}'.format('', \
                                                      width=(self.pageWidth - 2), \
                                                      fill=fillWith, \
                                                      c= self._CORNER_PLACEHOLDER)
        return outputLine
    
class SalesBill(Bill):
    
    _pageWidth = _PAGE_WIDTH
    _headers = ['Seller name', 'Number of sales', 'Total Value ($)']
    
    def __init__(self):
        Bill.__init__(self, SalesBill._pageWidth, SalesBill._headers)
        
    def Footer(self, totalSales, totalValue):
        outputLine = '{v}{:>{w[0]}}{v}{:>{w[1]}}{v}{:>{w[2]}.2f}{v}'\
            .format('Total:', totalSales, totalValue, w=self.columnWidths, v=_VERTICAL_PLACEHOLDER)
        return outputLine
    
    def Header(self):
        outputLine = '{v}{:<{w[0]}}{v}{:>{w[1]}}{v}{:>{w[2]}}{v}'\
            .format(*self.headers, w=self.columnWidths, v=_VERTICAL_PLACEHOLDER)
        return outputLine
    
    def ReportLine(self, item):
        outputLine = '{v}{:<{w[0]}}{v}{:>{w[1]}}{v}{:>{w[2]}.2f}{v}'\
            .format(*item, w=self.columnWidths, v=_VERTICAL_PLACEHOLDER)
        return outputLine
    
    def SalesBillOutput(self, reportData):
        output = []
        output.append(self.HorizontalLine(_HORIZONTAL_PLACEHOLDER_THICK))
        output.append(self.Header())
        output.append(self.HorizontalLine(_HORIZONTAL_PLACEHOLDER_THIN))
        for item in reportData['sales']:
            output.append(self.ReportLine(item))
        output.append(self.HorizontalLine(_HORIZONTAL_PLACEHOLDER_THIN))
        output.append(self.Footer(reportData['totalSales'], reportData['totalValue']))
        output.append(self.HorizontalLine(_HORIZONTAL_PLACEHOLDER_THICK))
        return output
    
class SaleBill(Bill):
    
    _pageWidth = _PAGE_WIDTH
    _headers = ['']
    
    def __init__(self):
        Bill.__init__(self, SaleBill._pageWidth, SaleBill._headers)
        
    def defineColumnsWidth(self, columns):
        columnWidths = []
        columnWidths.append(len(columns[0]))
        columnWidths.append((self.pageWidth - columnWidths[0]-3))
        return columnWidths
        
    def Salesman(self, user):
        columnWidths = self.defineColumnsWidth(user)
        outputLine = '{v}{:<{w[0]}}:{:{fill}>{w[1]}}{v}'\
            .format(*user, fill='', v=_VERTICAL_PLACEHOLDER, w=columnWidths)
        return outputLine
    
    def Beverage(self, beverage):
        columns = []
        columns.append('Beverage')
        columns.append(beverage)
        columnWidths = self.defineColumnsWidth(columns)
        outputLine = '{v}{:<{w[0]}}:{:{fill}>{w[1]}}{v}'\
            .format(*columns, fill='', v=_VERTICAL_PLACEHOLDER, w=columnWidths)
        return outputLine
    
    def Ingridient(self, ingridient):
        columnWidth = self.pageWidth - 2
        outputLine = '{v}{:>{width}}{v}'\
            .format(*ingridient, v=_VERTICAL_PLACEHOLDER, width=columnWidth)
        return outputLine
    
    def Price(self, price):
        columns = []
        columns.append('Total')
        columns.append(price)
        columnWidths = self.defineColumnsWidth(columns)
        #columnWidths[1] = columnWidths[1]- 2
        outputLine = '{v}{:>{w[0]}}:{:{fill}>{w[1]}.2f}{v}'\
            .format(*columns, fill='', v=_VERTICAL_PLACEHOLDER, w=columnWidths)
        return outputLine
    
    def SaleBillOutput(self, reportData):
        output = []
        output.append(self.HorizontalLine(_HORIZONTAL_PLACEHOLDER_THICK))
        output.append(self.Salesman(reportData['user']))
        output.append(self.HorizontalLine(_HORIZONTAL_PLACEHOLDER_THIN))
        output.append(self.Beverage(reportData['beverage']))
        for i in reportData['ingridients']:
            output.append(self.Ingridient(i))
        output.append(self.HorizontalLine(_HORIZONTAL_PLACEHOLDER_THIN))
        output.append(self.Price(reportData['price']))
        output.append(self.HorizontalLine(_HORIZONTAL_PLACEHOLDER_THICK))
        return output
        
    
