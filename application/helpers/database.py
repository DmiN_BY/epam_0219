#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on Feb 24, 2019
@author Dmitrij Nepachalovich
'''

import sqlite3
import paths as path_helper

DB_NAME = 'coffeeforme'

#sql_drop_table = "DROP TABLE %s"
#sql_create_table = "CREATE TABLE %s (%s)"
#sql_update_table = "INSERT OR IGNORE INTO %s (%s) VALUES (%s)"
#sql_select_from_table = "SELECT %s FROM %s WHERE %s='%s'"

class Table:
    def __init__(self, name, sql_columns_create, sql_columns_update):
        self.name = name
        self.sql_for_columns_creating = sql_columns_create
        self.set_columns_for_update = sql_columns_update
        
usersTable = Table('users', 'user_id INTEGER PRIMARY KEY NOT NULL, user TEXT NOT NULL UNIQUE, user_role TEXT NOT NULL', 'user, user_role')
beveragesTable = Table('beverages', 'beverage_id INTEGER PRIMARY KEY NOT NULL, beverage TEXT NOT NULL UNIQUE', 'beverage')
#ingridientsTable = Table('ingridients', 'ingridient_id INTEGER PRIMARY KEY NOT NULL, ingridient TEXT NOT NULL UNIQUE', 'ingridient')
salesTable = Table('sales', 'sale_id INTEGER PRIMARY KEY NOT NULL, user_id INTEGER, beverage_id INTEGER, price REAL', '')
sale_ingidientsTable = Table ('sale_ingridients', 'sale_id INTEGER NOT NULL, ingridient TEXT NOT NULL', 'sale_id, ingridient')

tables = (usersTable, beveragesTable, salesTable, sale_ingidientsTable)

class DbConn():
    def __init__(self):
        path_helper.setPathToDb()
        try:
            self._db_conn = sqlite3.connect(DB_NAME)
            self._db_cur = self._db_conn.cursor()
        except Exception as e:
            print (e)
        return None
    
    def __del__(self):
        try:
            self._db_conn.close()
        except Exception as e:
            print (e)
        return None

    def createTables(self):
        # create tables in db
        sql_create_table = "CREATE TABLE %s (%s)"
        try:
            for table in tables:
                self._db_cur.execute(sql_create_table % (table.name, table.sql_for_columns_creating))
            self._db_conn.commit()
        except Exception as e:
            print (e)
        return None
    
    def deleteTables(self):
        # deleting all tables in db
        sql_drop_table = "DROP TABLE %s"
        try:
            for table in tables:
                self._db_cur.execute(sql_drop_table % (table.name))
            self._db_conn.commit()
        except Exception as e:
            print (e)
        return None
    
    def updateTable(self, table, values):
        # add record to table if record not exists in table
        sql_update_table = "INSERT OR IGNORE INTO %s (%s) VALUES (%s)"
        try:
            self._db_cur.execute(sql_update_table % (table.name, table.set_columns_for_update, values))
            self._db_conn.commit()
        except Exception as e:
            print (e)
        return None

    def updateUsersTable(self, user, role):
        values = '\'%s\', \'%s\'' % (user, role)
        self.updateTable(usersTable, values)

    def updateBeveragesTable(self, beverage):
        value = '\'%s\'' % (beverage)
        self.updateTable(beveragesTable, value)
        
    def createNewSale(self, Sale, User):
        sql_select_from_table = "SELECT %s FROM %s WHERE %s='%s'"
        try:
            self._db_cur.execute(sql_select_from_table % ('user_id', usersTable.name, 'user', User.name))
            userId = self._db_cur.fetchone()[0]
            self._db_cur.execute(sql_select_from_table % ('beverage_id', beveragesTable.name, 'beverage', Sale.beverage))
            beverageId = self._db_cur.fetchone()[0] 
            self._db_cur.execute("INSERT INTO sales (user_id, beverage_id, price) VALUES ('%d', '%d', '%i')" % (userId, beverageId, Sale.price))
            saleId = self._db_cur.lastrowid
            for ing in Sale.ingridients:
                self._db_cur.execute("INSERT INTO sale_ingridients (sale_id, ingridient) VALUES ('%d', '%s')" % (saleId, ing))
            self._db_conn.commit()
        except Exception as e:
            print (e)
        return None
    
    def selectSales(self):
        try:
            self._db_cur.execute(
                                'SELECT user, COUNT(sale_id), SUM(price) FROM {sales} \
                                LEFT JOIN {users} ON {sales}.user_id = {users}.user_id \
                                GROUP BY user'\
                                .format(sales=salesTable.name, users=usersTable.name))
            sales = self._db_cur.fetchall()
            #print (sales)
        except Exception as e:
            print (e)
            return None
        else:
            return sales
        
    def selectTotalValue(self):
        try:
            self._db_cur.execute('SELECT SUM(price) FROM %s' % salesTable.name)
            totalValue = self._db_cur.fetchone()[0]
        except Exception as e:
            print (e)
            return None
        else:
            return totalValue
        
    def selectTotalSales(self):
        try:
            self._db_cur.execute('SELECT COUNT(sale_id) FROM %s' % salesTable.name)
            totalSales = self._db_cur.fetchone()[0]
        except Exception as e:
            print (e)
            return None
        else:
            return totalSales  
        
    def selectUserBySale(self):
        try:
            self._db_cur.execute('SELECT user_role, user FROM {sales} \
                                LEFT JOIN {users} ON {sales}.user_id = {users}.user_id \
                                WHERE sale_id = (SELECT max(sale_id) FROM {sales})'\
                                .format(sales=salesTable.name, users=usersTable.name))
            user = self._db_cur.fetchone()
        except Exception as e:
            print (e)
            return None
        else:
            return user  

    def selectBeverageBySale(self):
        try:
            self._db_cur.execute('SELECT beverage FROM {sales} \
                                LEFT JOIN {beverages} ON {sales}.beverage_id = {beverages}.beverage_id \
                                WHERE sale_id = (SELECT max(sale_id) FROM {sales})'\
                                .format(sales=salesTable.name, beverages=beveragesTable.name))
            beverage = self._db_cur.fetchone()[0]
        except Exception as e:
            print (e)
            return None
        else:
            return beverage
        
    def selectIngridientsBySale(self):
        try:
            self._db_cur.execute('SELECT ingridient FROM {ingridients} \
                                WHERE sale_id = (SELECT max(sale_id) FROM {sales})'\
                                .format(sales=salesTable.name, ingridients=sale_ingidientsTable.name))
            ingridients = self._db_cur.fetchall()
        except Exception as e:
            print (e)
            return None
        else:
            return ingridients 
    
    def selectSalePrice(self):
        try:
            self._db_cur.execute('SELECT price FROM {sales} \
                                WHERE sale_id = (SELECT max(sale_id) FROM {sales})'\
                                .format(sales=salesTable.name))
            price = self._db_cur.fetchone()[0]
        except Exception as e:
            print (e)
            return None
        else:
            return price
    


    