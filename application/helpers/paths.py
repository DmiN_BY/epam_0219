#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on Feb 24, 2019
@author Dmitrij Nepachalovich
'''

import os
from datetime import datetime
#from application import ap

BILLS_PATH = 'output'
DB_PATH = 'data'
APP_PATH = ''

def defineAppPath():
    global APP_PATH
    APP_PATH = os.getcwd()  

def definePath(pathTo):
    newPath = os.path.join(APP_PATH, pathTo)
    if not os.path.isdir(newPath):
        os.mkdir(newPath)
    return newPath

def setPathToBills():
    os.chdir(definePath(BILLS_PATH))
    
def setPathToDb():
    os.chdir(definePath(DB_PATH))
    
def getBillFilename():
    filename = 'bill'
    timestamp = '{:_%Y-%m-%d_%H-%M}'.format(datetime.now())
    extension = '.txt'
    fullFilename = '{}{}{}'.format(filename, timestamp, extension)
    return fullFilename